terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  token     = "__secret__"
  cloud_id  = "__secret__"
  folder_id = "__secret__"
  zone      = "ru-central1-a"
}

resource "yandex_iam_service_account" "ig-sa" {
  name        = "ig-sa"
  description = "service account to manage IG"
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  folder_id = "__secret__"
  role      = "editor"
  members   = [
    "serviceAccount:${yandex_iam_service_account.ig-sa.id}",
  ]
}

resource "yandex_compute_instance_group" "ig-1-nginx" {
  name                = "fixed-ig-with-balancer"
  folder_id           = "b1gbd7opign7umspm99b"
  service_account_id  = "${yandex_iam_service_account.ig-sa.id}"
  # prevents permission error when destroying  
  depends_on          = [yandex_resourcemanager_folder_iam_binding.editor]
  instance_template {
    platform_id = "standard-v3"
    resources {
      memory = 1
      cores  = 2
      core_fraction = 20
    }
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = "fd8fqbcrjab2nphrs4mk"        
      }
    }
    network_interface {
      network_id = "${yandex_vpc_network.network-1.id}"
      subnet_ids = ["${yandex_vpc_subnet.subnet-1.id}", "${yandex_vpc_subnet.subnet-2.id}"]
      nat = true
    } 
    metadata = {      
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
    network_settings {
      type = "STANDARD"
    }
    scheduling_policy {
      preemptible = true
    }
  }
  
  scale_policy {
    fixed_scale {
      size = 2
    }
  }
  allocation_policy {
    zones = ["ru-central1-a", "ru-central1-b"]
  }

  deploy_policy {
    max_unavailable = 1
    max_creating    = 1
    max_expansion   = 0
    max_deleting    = 1
  } 

  load_balancer {
    target_group_name        = "ig-1-nginx-lb"
    target_group_description = "load balancer ig-1-nginx-lb"
  }
}

resource "yandex_lb_network_load_balancer" "ig-1-nginx-lb" {
  name = "nginx-ig-1-nginx-lb"  
  listener {
    name = "ig-1-nginx-lb-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_compute_instance_group.ig-1-nginx.load_balancer[0].target_group_id}"

    healthcheck {
      name = "http"
      http_options {
        port = 80        
      }
    }
  }
}

resource "yandex_compute_instance" "web-react-1" {
  name = "web-react-1"

  resources {
    cores  = 2
    memory = 1
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = "fd82re2tpfl4chaupeuf"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }

  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

# resource "yandex_vpc_security_group" "network-1-sg" {
#   name        = "My security group"
#   network_id  = "${yandex_vpc_network.network-1.id}"

#   dynamic "ingress" {    
#     for_each = ["22", "80"]
#     content {
#       port        = ingress.value 
#       v4_cidr_blocks = ["0.0.0.0/0"]
#       protocol    = "tcp"      
#     }
#   } 

#   egress {
#     protocol       = "ANY"
#     v4_cidr_blocks = ["0.0.0.0/0"]  
#     port           = 0
#   } 
# }

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.network-1.id}"
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_vpc_subnet" "subnet-2" {
  name           = "subnet2"
  zone           = "ru-central1-b"
  network_id     = "${yandex_vpc_network.network-1.id}"
  v4_cidr_blocks = ["192.168.11.0/24"]
}

output "web_loadbalancer_ip" {
  value = "${yandex_lb_network_load_balancer.ig-1-nginx-lb.listener.*.external_address_spec}"
}

output "network_interface_nginx_ig" {
  value = "${yandex_compute_instance_group.ig-1-nginx.instances.*.network_interface}"
}

output "external_ip_address_web-react-1" {
  value = "${yandex_compute_instance.web-react-1.network_interface.0.nat_ip_address}"
}
